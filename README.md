## Soap server

#### monfee
Link:
/sclip/web-service/monfee?wsdl

#### subscribe
Link:
/sclip/web-service/subscribe?wsdl

#### mo-listener
Link:
/sclip/web-service/mo-listener?wsdl

#### get-content
Link:
/sclip/web-service/get-content?wsdl

## Export via ssh

#### Setting 
Setting on application.properties:

* shell.auth.simple.user.name=root
* shell.auth.simple.user.password=H0Huy@123
* shell.disabled-commands=jpa*,jdbc*,jndi*,repl*,autoconfig*,beans*,cron*,dashboard*,egrep*,endpoint*,env*,filter*,java*,jmx*,jul*,jvm*,less*,mail*,metrics*,sleep*,sort*,system*,thread*
* shell.ssh.port=2000
* shell.telnet.enabled=true
* shell.telnet.port=5000

Refer to more setting

* [CRaSH](http://www.crashub.org/1.3/reference.html)
* [common-application-properties](http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html)

#### Use

**Connection:**

```
ssh -p 2000 root@localhost
```

**Command:**

* help
* man
* export_getcontent
	1. today
	2. onday
	3. period
* export_molistener
	1. today
	2. onday
	3. period
* export_resultnew
	1. today
	2. onday
	3. period
* export_subscribe
	1. today
	2. onday
	3. period

 




