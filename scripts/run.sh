#!/bin/bash
CONFIG=$1

OPT=""
if [ -e /root/config/$CONFIG ]; then
   OPT="--spring.config.location=/root/config/$CONFIG"
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DIR=/root
cd $DIR

MAIN_CLASS="com.hohuy.mps.MonfeeApplication $DIR"
echo $MAIN_CLASS
########################################################
#                  BUILDING CLASSPATH                  #
########################################################
CLASS_PATH="$DIR/resource";
for file in `ls ext-lib/*.jar`
do
	CLASS_PATH="$CLASS_PATH:$DIR/$file"
done

for file in `ls lib/*.jar`
do
	CLASS_PATH="$CLASS_PATH:$DIR/$file"
done

CLASS_PATH="$CLASS_PATH:$DIR/monfee-0.0.3-SNAPSHOT.jar.original"
echo $JAVA_HOME/bin/java -cp $CLASS_PATH $JVM_PARAMS $MAIN_CLASS $OPT
$JAVA_HOME/bin/java -cp $CLASS_PATH $JVM_PARAMS $MAIN_CLASS $OPT
