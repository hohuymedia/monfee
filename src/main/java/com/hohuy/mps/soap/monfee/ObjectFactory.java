
package com.hohuy.mps.soap.monfee;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cm.cw package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ResultRequestResponse_QNAME = new QName("http://ws.com/", "resultRequestResponse");
    private final static QName _ResultRequest_QNAME = new QName("http://ws.com/", "resultRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cm.cw
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResultRequest }
     * 
     */
    public ResultRequest createResultRequest() {
        return new ResultRequest();
    }

    /**
     * Create an instance of {@link ResultRequestResponse }
     * 
     */
    public ResultRequestResponse createResultRequestResponse() {
        return new ResultRequestResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.com/", name = "resultRequestResponse")
    public JAXBElement<ResultRequestResponse> createResultRequestResponse(ResultRequestResponse value) {
        return new JAXBElement<ResultRequestResponse>(_ResultRequestResponse_QNAME, ResultRequestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.com/", name = "resultRequest")
    public JAXBElement<ResultRequest> createResultRequest(ResultRequest value) {
        return new JAXBElement<ResultRequest>(_ResultRequest_QNAME, ResultRequest.class, null, value);
    }

}
