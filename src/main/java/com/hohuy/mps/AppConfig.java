/**
 * 
 */
package com.hohuy.mps;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author giangdd
 *
 */
@Component
@Scope("singleton")
public class AppConfig implements Serializable {
	private static final long serialVersionUID = -7605681936529899163L;
	private Logger logger = Logger.getLogger(AppConfig.class);
	private static AppConfig appConfig;

	/**
	 * Platform ShortCode
	 */
	@Value("${vtl.mps.shortcode}")
	private String smsShortcode;
	@Value("${vtl.mps.monfee.url}")
	private String monfeeUrl;
	
	/**
	 * Platform Monfee Username & Password
	 */
	@Value("${vtl.mps.monfee.username}")
	private String monfeeUsername;
	@Value("${vtl.mps.monfee.password}")
	private String monfeePassword;
	
	@Value("${vtl.mps.monfee.service}")
	private String serviceID;
	
	@Value("${vtl.mps.monfee.amount:1000}")
	private String amount;

	private AppConfig() {
	}

	public static AppConfig getInstance() {
		return appConfig;
	}

	@PostConstruct
	private void load() {
		logger.info("========== SHORTCODE: " + this.smsShortcode + "==============");
		logger.info("========== MONFEE: " + this.monfeeUsername + "/" + this.monfeePassword + "==============");
	}


	public String getUrl() {
		return monfeeUrl;
	}

	public String getShortcode() {
		return smsShortcode;
	}

	public String getUsername() {
		return monfeeUsername;
	}

	public String getPassword() {
		return monfeePassword;
	}

	public String getSmsShortcode() {
		return smsShortcode;
	}

	public void setSmsShortcode(String smsShortcode) {
		this.smsShortcode = smsShortcode;
	}

	public String getMonfeeUrl() {
		return monfeeUrl;
	}

	public void setMonfeeUrl(String monfeeUrl) {
		this.monfeeUrl = monfeeUrl;
	}

	public String getMonfeeUsername() {
		return monfeeUsername;
	}

	public void setMonfeeUsername(String monfeeUsername) {
		this.monfeeUsername = monfeeUsername;
	}

	public String getMonfeePassword() {
		return monfeePassword;
	}

	public void setMonfeePassword(String monfeePassword) {
		this.monfeePassword = monfeePassword;
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
}
