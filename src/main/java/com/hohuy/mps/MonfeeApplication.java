package com.hohuy.mps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MonfeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonfeeApplication.class, args);
	}
}
