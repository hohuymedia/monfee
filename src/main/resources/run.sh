#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

MAIN_CLASS="com.hohuy.mps.MpsApplication $DIR"
echo $MAIN_CLASS
########################################################
#                  BUILDING CLASSPATH                  #
########################################################
CLASS_PATH="$DIR/resource";
for file in `ls ext-lib/*.jar`
do
	CLASS_PATH="$CLASS_PATH:$DIR/$file"
done

for file in `ls lib/*.jar`
do
	CLASS_PATH="$CLASS_PATH:$DIR/$file"
done

CLASS_PATH="$CLASS_PATH:$DIR/mps-0.0.2-SNAPSHOT.jar.original"
echo $JAVA_HOME/bin/java -cp $CLASS_PATH $JVM_PARAMS $MAIN_CLASS 
$JAVA_HOME/bin/java -cp $CLASS_PATH $JVM_PARAMS $MAIN_CLASS 

